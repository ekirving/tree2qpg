# README #

Author: EK Irving-Pease

tree2qpg is a simple python script for automatically converting a [Treemix](https://bitbucket.org/nygcresearch/treemix)
tree topology with migration branches into the qpGraph format used by [AdmixTools](https://github.com/DReichLab/AdmixTools)


### Example usage ###

```bash
# convert Treemix to qpGraph
python tree2qpg.py treemix/test.m3.treeout.gz  > qpgrap/test.m3.graph
```

```bash
# run qpGraph
qpGraph -p qpgraph/test.par -g qpgraph/test.m3.graph -d qpgraph/test.m3.dot
```


```bash
# convert to postscript for viewing
dot -Tps qpgraph/test.m3.dot > qpgraph/test.m3.ps
```